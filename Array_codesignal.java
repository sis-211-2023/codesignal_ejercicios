/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.array_codesignal;

/**
 *
 * @author Windows 10
 */

public class Array_codesignal {

    public static void main(String[] args) {
        int solution(int[] inputArray) {
int maxProduct = inputArray[0] * inputArray[1];
for (int pos =  1; pos < inputArray.length-1 ; pos++){
    if(maxProduct < inputArray[pos] * inputArray[pos + 1]){
    maxProduct = inputArray[pos] * inputArray[pos + 1];
    }
    }
    return maxProduct;
}

