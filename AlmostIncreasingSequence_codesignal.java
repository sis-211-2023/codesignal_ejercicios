/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.almostincreasingsequence_codesignal;

/**
 *
 * @author Windows 10
 */
public class AlmostIncreasingSequence_codesignal {
public static void main(String[] args)
    boolean solution(int[] sequence) {
 int size = sequence.length; 
    int counter = 0;
    
    if (size <= 2) {
        return true; 
    }
    
    for (int pos = 0; pos < size - 1; pos++) {
        if (sequence[pos + 1] <= sequence[pos]) { 
            counter++;
            boolean skipNeighbor = pos + 2 < size && sequence[pos + 2] <= sequence[pos];
            boolean skipBack = pos - 1 >= 0 && sequence[pos + 1] <= sequence[pos - 1];            
            if (skipNeighbor && skipBack) {
                return false;
            }
        }
    }
    
    return true; 
}
}
